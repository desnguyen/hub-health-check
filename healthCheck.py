from urllib import request
import requests
import sys
import yaml
import datetime
import json
import xlsxwriter
from ucbCheck import *

def check_connectivity(url, credentials, edge):
    status = None
    print('Checking connectivity')
    headers = {
        'Authorization': f'Basic {credentials}'
    }

    try:
        response = requests.get(f'{url}/api/read/connstatus?edge={edge}', headers=headers)
        if response.status_code == 200:
            jsonResp = json.loads(response.text)
            status = jsonResp['data']['brokerSubscriptionStatus']
            return status

        else:
            print('ERROR: Unable to get connectivity status')
    except:
        print('ERROR: Unable to call API to get status')


def get_ecs_token(auth_url, client_id, client_secret):
    auth_headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cache-Control': 'no-store',
    }
    payload = {
        'client_id': client_id,
        'client_secret': client_secret,
        'grant_type': 'client_credentials'
    }

    access_token = None

    try:
        auth_response = requests.post(auth_url, headers=auth_headers, data=payload, timeout=60)
        if auth_response.status_code == 200 or auth_response.status_code == 201:
            jsonResp = json.loads(auth_response.text)
            access_token = jsonResp['access_token']
            print('Able to obtain access token')
        else:
            print("ERROR: Cannot obtain access token: " + auth_response.text)
    except requests.exceptions.RequestException as e:
        print("ERROR: Cannot Obtain auth token: " + str(e))

    return access_token

def get_on_demand_hr(token, url, edge):
    jsonResp = {}
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.post(f'{url}{edge}?commandId=get-health-report&mode=sync', headers=headers)
        #print(f'{response.text}\n')
        jsonResp = json.loads(response.text)
    except:
        print(f'ERROR: Cannot get health report for edge {edge}')

    #print(f'Getting On-Demand Health Report for {edge}')

    return jsonResp

def get_fw_version(token, url, edge):
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.post(f'{url}{edge}?commandId=get_firmware_version&mode=sync', headers=headers)
    except:
        print(f'ERROR: Cannot get fw version for edge {edge}')
        respose = ''

    return response.text    

def write_headers(ws):
    ws.write('A1', 'Edge')
    ws.write('B1', 'edge_type')
    ws.write('C1', 'comm_status')
    ws.write('D1', 'fw_version')
    ws.write('E1', 'uptime')
    ws.write('F1', 'EUI')

def get_edge_type(edge):
    #IPQ
    if edge[6] == '0':
        print(f'{edge} == IPQ')
        return 0
    # APQ
    elif edge[6] == '4':
        print(f'{edge} == APQ')
        return 1
    else:
        print(f'ERROR: Edge {edge} not recognized')
        return 2

def get_ucb_eui(url, credentials, edge):
    eui = None
    print('Getting EUI')
    headers = {
        'Authorization': f'Basic {credentials}'
    }

    try:
        response = requests.get(f'{url}/api/read/edges?id={edge}', headers=headers)
        if response.status_code == 200:
            jsonResp = json.loads(response.text)
            for edge in jsonResp:
                eui = jsonResp['edges'][0]['access']['ucb_dev_eui']
                print(eui)
            return eui

        else:
            print('ERROR: Unable to get eui')
    except:
        print('ERROR: Unable to call API to get EUI')


if __name__ == "__main__":
    # Checking for the correct num of params
    if len(sys.argv) < 2:
        print('Usage python3 <py file> <config file> <device list file>...')
        exit(-1)

    try:
        in_args = yaml.safe_load(open(sys.argv[1]))
        if len(sys.argv) > 2:
            inputFileName = sys.argv[2]
            inputList = open(inputFileName, 'r')
            # need to fix to give it a more uniquie name or something
            outputFileName = xlsxwriter.Workbook(f"{inputFileName}.csv")
            ws = outputFileName.add_worksheet()
            write_headers(ws)

        else:
            print('Not enough input -- exiting')
            exit(-1)
    except OSError as e:
        print(f'Initialization ERROR: {e}')  

    # Getting token
    ecs_token = get_ecs_token(in_args['auth_url'], in_args['client_id'], in_args['client_secret'])

    # writing data into an excel sheet
    # i is to keep track of the row
    i = 2
    for edge in inputList:
        edge = edge.replace('\n', '').rstrip()
        ws.write(f'A{i}', edge)
        # Getting edge type
        type = get_edge_type(edge)
        if type == 0:
            ws.write(f'B{i}', 'IPQ')
        elif type == 1:
            ws.write(f'B{i}', 'APQ')
        else:
            ws.write(f'B{i}', '???')
        # Checking connectivity
        status = check_connectivity(in_args['config_url'], in_args['config_cred'], edge)
        ws.write(f'C{i}', status)
        if status == 'ONLINE':
            # Getting health report
            hr = get_on_demand_hr(ecs_token, in_args['ecs_url'], edge)
            # getting fw version
            edge_fw = get_fw_version(ecs_token, in_args['ecs_url'], edge)
            ws.write(f'D{i}', edge_fw)
            # uptime
            ws.write(f'E{i}', hr['os']['component']['uptime'])
        else:
            print('Nothing to do - device offline')
        if type == 0:
            eui = get_ucb_eui(in_args['config_url'], in_args['config_cred'], edge)
            ws.write(f'F{i}', eui)
            # checking UCB
            test = ubicellMQTT(in_args['ucb_server'], eui)
        else:
            ws.write(f'F{i}', 'N/A')
        i = i + 1

    outputFileName.close()