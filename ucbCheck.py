"""
Main purpose of this file is to run the method 'ubicellMqtt' which will:
- connect to both ubicell/EUI/tx and ubicell/EUI/rx
- Subscribe to both topics
- publish the given t command
- return the received message based off of the t command
"""
import paho.mqtt.client as mqtt
import time
import codecs
import subprocess

# global variable to retrieve mqtt message outside of on_message function
global_encoded_pMsg = 'nothing'

def on_log(client, userdata, level, buffer):
    print(f'log: {buffer}')


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print('Connection successful')
    else:
        print('Connection unsuccessful, return code: ', str(rc))
    


def on_disconnect(client, userdata, flags, rc=0):
    print('Disconnected, return code: ', str(rc))


def on_message(client, userdata, msg):
    global global_encoded_pMsg
    # print(f'topic: {msg.topic}')
    # encode to hex, then decode to utf-8 to get rid of the 'b' bytes
    global_encoded_pMsg = str(codecs.encode(msg.payload, "hex").decode("utf-8"))
    #print(f'encoded message received: {global_encoded_pMsg}')

    return global_encoded_pMsg


def ubicellMQTT(server, EUI):
    CMD_topic = 'ubicell/%s/tx' % EUI 
    STATUS_topic = 'ubicell/%s/rx' % EUI 
    # command is in Base64
    command = 'dDo4OQ=='
    publish_msg = '{"payload":"%s"}' % str(command)
    
    # constructor: creating a client
    client = mqtt.Client()

    # creating instances
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_log = on_log
    client.on_message = on_message
    client.username_pw_set(username="test",password="test")

    # connect to broker
    client.connect(server)
    print(f'Connecting to {server}....')
    # begin async subscription
    client.loop_start()

    # listening to nodes
    # subscribe to command topic
    client.subscribe(CMD_topic)
    # subscribe to status topic
    client.subscribe(STATUS_topic)
    # publishing message; sending t-command
    client.publish(CMD_topic, publish_msg)
    # send and wait for status topic to receive the message, will wait max 30 sec
    tries = 0
    while len(global_encoded_pMsg) < 250 and tries <= 5:
        time.sleep(6)
        tries = tries + 1
    client.loop_stop()
    client.disconnect()

    return global_encoded_pMsg

def decode_ubilpp(hex_msg):
    decoded_msg = subprocess.run(['node', 'ubilpp-decode.js', hex_msg], stdout=subprocess.PIPE)
    print(decoded_msg.stdout)

