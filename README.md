# Read Me

Following packages will need to be installed:
- pip install xlsxwriter 
- pip install paho-mqtt
Note: pip3 can also be used depending on version

Run `python3 healthCheck.py <yml file> <list of edges>`

Need a yml configuration, template as follows:
{
  'auth_url': '<auth_url>',
  'client_id': 'ie_client',
  'client_secret': '<client_secret>',
  'ecs_url': '<ecs_url>',
  'config_url': '<apiDoc config_url>',
  'config_cred': '<config_cred in base64>',
  'ucb_server': '<broker for ucb mqtt>'
}

Edge list to check can contain both IPQs and APQs, where each edge is seperated by a new line
Ex:
947bbe001c3c
947bbe400086
947bbe401b26
947bbe003698